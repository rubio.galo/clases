
## Operaciones
- Suma (+)
```php
2 + 3
------
5
```
- Resta (-)
```php
2 - 3
------
-1
```
- Multiplicación (*)
```php
2 * 3
------
6
```
- División (/)
```php
2 / 3
------
0.6667
```
- Residuo (%)
```php
3 % 2
------
1
```
- Concatenación ( . )
```php
$nombre = "Galo"
"hola" . "mi nombre es " . $nombre
-----------------------------------
"hola mi nombre es Galo"
```
