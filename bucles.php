<?php
/*
Estructuras Condicionales
Operadores de comparación < > <= >= == !=

Sentencias if, else, else if
Operadores lógicos  &&(y) ||(ó) !(no)
Sentencias switch if(x==1,x==2,x==3)

Estructuras de iteración
Ciclo while(mientras)
Ciclo for(para)
Ciclo for each (para cada elemento)
*/

//IF

$x = 3;
$y = 2;

if($x<$y)
{
    echo "x es menor que y";
}
else if($x==$y)
{
    echo "x es igual que y";
}
else
{
    echo "x es mayor que y";
}

$nombre="Juan";

if($nombre=="Juan")
{
    echo "Hola Juan";
}
else if($nombre=="Pedro")
{
    echo "Hola Pedro";
}
else if($nombre=="Andrea")
{
    echo "Hola Andrea";
}
else
{
    echo "Hola desconocido";
}

switch($nombre)
{
    case "Juan":
        echo "Hola Juan";
        break;
    case "Pedro":
        echo "Hola Pedro";
        break;
    case "Andrea":
        echo "Hola Andrea";
        break;
    default:
        echo "Hola desconocido";
        break;
}

//WHILE

$i = 0;
while($i<10)
{
    echo $i;
    $i++;
}
// 0 1 2 3 4 5 6 7 8 9

$ok=false;

while($ok)
{
    echo "Hola";
    //if(cliente preiono 0){
        //$ok=false;
    //}    
    $ok=false;
}

//ejecuto 1 vez sin comparar
do {
    echo "Hola";
    $ok=false;
} while($ok);

//FOR(var inicio ;)
for($i=0;$i<10;$i=$i+1)
{
    echo $i;
}

//FOR EACH (arreglo as varible interna)
$array = [
    "Juan",//0
    "Pedro",//1
    "Andrea",//2
];

echo $array[0];
echo $array[1];
echo $array[2];

forEach($array as $posicion)
{
    echo $posicion;
}