## Función var_dump
Esta función muestra información estructurada sobre una o más expresiones incluyendo su tipo y valor.

```php
<?php
$a = array(1, 2, array("a", "b", "c"));
var_dump($a);
?>
```
Resultado
```
array(3) {
  [0]=>
  int(1)
  [1]=>
  int(2)
  [2]=>
  array(3) {
    [0]=>
    string(1) "a"
    [1]=>
    string(1) "b"
    [2]=>
    string(1) "c"
  }
}
```

## Función echo
Esta función muestra información estructurada sobre una o más expresiones incluyendo su tipo y valor.

```php
<?php
echo "Hola mundo";
?>
```
Resultado
```
Hola mundo
```


# Ejercicios
1. Almacenar dos variables, realizar las operaciones básicas y mostrar el resultado de la siguiente manera (x=3, y=2 ):
```
Operaciones Básicas:

Suma: 3 + 2 = 5
Resta: 3 - 2 = 1
Multiplicación: 3 * 2 = 6
División: 3 / 2 = 1.5
Residuo: 1 % 2 = 1

```

2. Utilizando el archivo pokemon.php responder las siguientes preguntas:
- ¿Qué tipo de dato es la variable $pokemons?
- ¿Cuantos elementos tiene results?
- Elegir un pokemon e imprimir utilizando variables: 