<?php
//TIPÓ DE DATOS
$intNumeroUno   =   1;      //Entero
$floatNumeroDos =   1.5;    //Flotante
$boolEsVerdad   =   true;   //Booleano
$strNombre      =   "Juan"; //String

$x = 1;

$arrayNormal    = [ // $array[]
    "Juan",     //[0]
    "Pedro",    //[1]
    "Maria",    //[2]
];

$arrayNormal[2]; // "Maria"

$arrayAsociociativo = [
    "nombre" => "Juan",
    "apellido" => "Perez",
    "edad" => 23,
    "genero" => "masculino",
];

$arrayAsociociativo["edad"];

// echo $arrayAsociociativo["edad"];
// echo "<br>";

// echo $floatNumeroDos;
// echo "<br>";

// //Que tiene la variable
// var_dump($arrayAsociociativo);
// echo "<br>";
// var_dump($x);

// //JSON
// echo "<br>";
// echo json_encode($arrayNormal);
// echo "<br>";
// echo json_encode($arrayAsociociativo);

// echo "<br>";
// echo "Hola Mundo";
// echo "<br>";
// echo $strNombre;

// //Concatenar

// $texto = "Hola mi nombe es " . $strNombre . " soy el numero " . $intNumeroUno;
// echo "<br>";
// echo $texto;
// echo "<br>";
// /* 
// suma + 
// resta - 
// multipicacion * 
// division / 
// residuo %
// */

// echo 3%2;