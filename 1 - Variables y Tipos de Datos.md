# Clases PHP

## Variables 

Para crear una variable se utiliza el simbolo $ antes del nombre de la variable.

Ejemplo
```php
$nombreVariable = ...
```

## Tipos de Datos
### Primitivos
- Entero (int)
- Flotante (float)
- Booleano (bool)
- Cadena de Texto (string)

```php
$intNumeroUno   =   1;      //Entero
$floatNumeroDos =   1.5;    //Flotante
$boolEsVerdad   =   true;   //Booleano
$strNombre      =   "Juan"; //String
```
### Compuestos
- Array []

```php

$arrayNormal    = [ // $array[]
    "Juan",     //[0]
    "Pedro",    //[1]
    "Maria",    //[2]
];

$arrayNormal[2]; // "Maria"
```
- Array Asociativo ("key" => "value")
```php
$arrayAsociociativo = [
    "nombre" => "Juan",
    "apellido" => "Perez",
    "edad" => 23,
    "genero" => "masculino",
];

$arrayAsociociativo["edad"];
```
- Object 

```php
class foo
{
    function hacer_algo()
    {
        echo "Haciendo algo."; 
    }
}

$bar = new foo;
$bar->hacer_algo();
```